# WebRTC
_déployer P2P dans le navigateur_

Stéphane Alnet
_shimaore_

### Plan
WebRTC
Déploiement

# WebRTC

WebRTC = *media capture* + peer-to-peer

## media capture
caméras
microphones
capture d'écran

```javascript
navigator.mediaDevices
….getUserMedia()
….getDisplayMedia()
```

```javascript
localStream = await getUserMedia(constraints)
localStream.getTracks()
```

### local playback
`<video autoplay playsinline controls="false">`
`video.srcObject` = MediaStream

WebRTC = media capture + *peer-to-peer*

```javascript
pc = new RTCPeerConnection({iceServers})
```

```javascript
offer = await pcA.createOffer()
// A → send({offer}) → B
…
await pcA.setLocalDescription(offer)
// pcA emits `icecandidate`
// A → send({icecandidate}) → B
```

```javascript
// {offer} → B
pcB.setRemoteDescription(offer)
…
answer = await pcB.createAnswer()
// B → send({answer}) → A
…
await pcB.setLocalDescription(answer)
// pcB emits `icecandidate`
// B → send({icecandidate}) → A
```

```javascript
// {answer} → A
pcA.setRemoteDescription(answer)
```

Add local tracks
```javascript
pc.addTrack(track,localStream)
```

Add remote tracks
```javascript
remoteStream = MediaStream()
video.srcObject = remoteStream
pc.addEventListener('track', ({track}) => {
  remoteStream.addTrack(track,remoteStream)
})
```

```
pc.createDataChannel(label)
```

```
addEventListener('open')
addEventListener('close')
addEventListener('message')
.send(message)
```

offer, answer ~ SDP

### SDP (RFC4566)
Session Description Protocol

```sdp
o=mozilla...THIS_IS_SDPARTA-69.0.2 7990647775988802219 0 IN IP4 0.0.0.0
s=-
t=0 0
a=sendrecv
a=fingerprint:sha-256 E8:2A:05:BB:FB:BB:69:B7:F2:73:70:06:02:82:1C:4C:44:AB:5B:2C:C6:53:5D:17:…
a=group:BUNDLE 0
a=ice-options:trickle
a=msid-semantic:WMS *
m=application 48991 UDP/DTLS/SCTP webrtc-datachannel
c=IN IP4 192.168.1.162
a=candidate:0 1 UDP 2122187007 192.168.1.162 48991 typ host
a=candidate:2 1 UDP 2122252543 2001:dead:beef:1:d828:1e0c:365c:1696 37429 typ host
a=candidate:4 1 TCP 2105458943 192.168.1.162 9 typ host tcptype active
a=candidate:5 1 TCP 2105524479 2001:dead:beef:1:d828:1e0c:365c:1696 9 typ host tcptype active
a=sendrecv
a=ice-pwd:e7a818d9284e3d0ca1f04f5a3db05453
a=ice-ufrag:0e2391c1
a=mid:0
a=setup:active
a=sctp-port:5000
a=max-message-size:1073741823
```

### Higher-level APIs
https://github.com/feross/simple-peer
https://peerjs.com/

### Synchronization
PouchDB-over-WebRTC
https://github.com/natevw/PeerPouch

# Déploiement

## Signaling channel

WebRTC fournit le media,
_pas_ la signalisation

### Fonctions de la signalisation
Registrar
Session setup

### Session setup (WebRTC)
offer
answer
ICE candidates

### Session setup (WebRTC)
WS, Socket.IO, …
PeerJS

## Déployer au-delà du web
## (et des apps)

## SIP
Session Initiation Protocol

SIP/WS (RFC7118)

### SIP/WS (frontend)
https://jssip.net/
https://sipjs.com/

### SIP/WS (backend)
OpenSIPS
FreeSwitch

## NAT traversal &
## Protocol conversion

### Problématiques
NAT (IPv4 - IPv4)
conversion (IPv4 - IPv6)
parefeu avec filtrage UDP

### Network Address Translation
plusieurs addresse privées (RFC1918)
cachées derrière une seule IP publique

### STUN

### STUN
Session Traversal Utilities for NAT
RFC 5389

### STUN
Essaie de deviner l'adresse IP
et le port (UDP) qui vont être utilisés

### TURN

### TURN
Traversal Using Relays around NAT
RFC5766

### TURN
Étend STUN
Utilise un relai
Le relai doit être situé sur l'Internet ouvert

## ICE

## ICE
Interactive Connectivity Establishment
RFC 8445

## ICE
Construit dynamiquement
une liste de destinations possibles

ICE
```sdp
a=candidate:0 1 UDP 2122187007 192.168.1.162 35734 typ host
a=candidate:3 1 UDP 2122252543 2001:dead:beef:1:d828:1e0c:365c:1696 35021 typ host
a=candidate:6 1 TCP 2105458943 192.168.1.162 9 typ host tcptype active
a=candidate:7 1 TCP 2105524479 2001:dead:beef:1:d828:1e0c:365c:1696 9 typ host tcptype active
.
.
.
```

ICE avec STUN
```sdp
a=candidate:0 1 UDP 2122187007 192.168.1.162 35734 typ host
a=candidate:3 1 UDP 2122252543 2001:dead:beef:1:d828:1e0c:365c:1696 35021 typ host
a=candidate:6 1 TCP 2105458943 192.168.1.162 9 typ host tcptype active
a=candidate:7 1 TCP 2105524479 2001:dead:beef:1:d828:1e0c:365c:1696 9 typ host tcptype active
a=candidate:1 1 UDP 1685987327 80.87.88.89 35734 typ srflx raddr 192.168.1.162 rport 35734
.
.
```

ICE avec TURN
```
a=candidate:0 1 UDP 2122187007 192.168.1.162 35734 typ host
a=candidate:3 1 UDP 2122252543 2001:dead:beef:1:d828:1e0c:365c:1696 35021 typ host
a=candidate:6 1 TCP 2105458943 192.168.1.162 9 typ host tcptype active
a=candidate:7 1 TCP 2105524479 2001:dead:beef:1:d828:1e0c:365c:1696 9 typ host tcptype active
a=candidate:1 1 UDP 1685987327 80.87.88.89 35734 typ srflx raddr 192.168.1.162 rport 35734
a=candidate:2 1 UDP 921518 164.171.159.251 54015 typ relay raddr 163.172.159.251 rport 54015
a=candidate:5 1 UDP 922173 164.171.159.251 58648 typ relay raddr 163.172.159.251 rport 58648
```

## Private ICE

### Pourquoi
le média est certes encrypté
mais un tiers est capable de savoir…

qui parle avec qui
quand
combien de temps
combien de fois
…

### Private ICE
Fin des relais publics

### coturn
https://github.com/coturn/coturn

### coturn
déployable en conteneur
derrière NAT de cloud
IPv4, IPv6, UDP, TCP
auth: statique, DB, REST, oAuth

# Conclusion
### _Credits_
_Patrick Mueller, Jordan Ladikos,
Michael Bourgault, Johan Arthursson … on unsplash_
