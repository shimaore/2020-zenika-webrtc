all: clean main.html

clean:
	rm -f main.html

main.html: main.md
	cat slide-head.html $< slide-bottom.html >$@
